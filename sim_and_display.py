#pythonnnn

'''
sim_and_display.py

this script is intended to open COVID19 data
formatted by the JHU folks and display along with
providing a very basic simulated model to compare
to the actuals
'''


###################################
#IMPORT MODULES
###################################
import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import dates



###################################
#USER INPUT FOR SIM
###################################
#R-naught, Wuhan thought to be 2.35 pre-quarantine: https://www.thelancet.com/journals/laninf/article/PIIS1473-3099(20)30144-4/fulltext
Ro = 2.35 

#number of days to simulate
days_to_run = 15

#number of beds in the healthcare system
healthcare_cap = 1000000 

#number of cases on day 1, can't be less than 1
cases_at_day1 = 1 

#percent of infected people that will require hospitalization, currently estimated at 20% https://www.vox.com/2020/3/12/21176783/coronavirus-covid-19-deaths-china-treatment-cytokine-storm-syndrome
pct_hostpitalization = 20

#days of hospitalization required for recovery, SWAG'd as 6?
hosp_days_req = 6


###################################
#MAIN CODE
###################################

#sim

#just shows completely uncontained spread among unlimited population
if cases_at_day1 < 1: cases_at_day1 = 1

tday = []
cum_num_cases = []
healthcare_capp = []
healthcare_use = []
for n in range(0,days_to_run):

    tday += [n]
    if n == 0:
        cum_num_cases += [cases_at_day1]
    elif n == 1:
        cum_num_cases += [cum_num_cases[-1]*Ro+cum_num_cases[-1]]
    else:
        cum_num_cases += [(cum_num_cases[-1]-cum_num_cases[-2])*Ro+cum_num_cases[-1]]

    healthcare_use += [cum_num_cases[-1]*(pct_hostpitalization/100)]

    if n < hosp_days_req:
        pass
    else:
        healthcare_use[-1] = healthcare_use[-1]-healthcare_use[-1*hosp_days_req]

    healthcare_capp += [healthcare_cap]


#load data
print('loading...')
confirmed_cases = pd.read_csv("time_series_covid19_confirmed_global.csv", encoding='utf-8-sig')
deceased_cases = pd.read_csv("time_series_covid19_deaths_global.csv", encoding='utf-8-sig')
recovered_cases = pd.read_csv("time_series_covid19_recovered_global.csv", encoding='utf-8-sig')

print('...complete')

#quiet warnings
pd.options.mode.chained_assignment = None

#clean up data
print('cleaning...')
confirmed_cases.drop("Lat",axis = 1, inplace=True)
confirmed_cases.drop("Long",axis = 1, inplace=True)
recovered_cases.drop("Lat",axis = 1, inplace=True)
recovered_cases.drop("Long",axis = 1, inplace=True)
deceased_cases.drop("Lat",axis = 1, inplace=True)
deceased_cases.drop("Long",axis = 1, inplace=True)

#extract data

#sum world
world_confirmed = confirmed_cases.copy()
world_confirmed.drop('Country/Region',axis = 1, inplace=True)
world_confirmed.drop('Province/State',axis = 1, inplace=True)
world_sum = world_confirmed.sum()
world_confirmed = world_sum.values.tolist()

world_recovered = recovered_cases.copy()
world_recovered.drop('Country/Region',axis = 1, inplace=True)
world_recovered.drop('Province/State',axis = 1, inplace=True)
world_sum = world_recovered.sum()
world_recovered = world_sum.values.tolist()

world_deceased = deceased_cases.copy()
world_deceased.drop('Country/Region',axis = 1, inplace=True)
world_deceased.drop('Province/State',axis = 1, inplace=True)
world_sum = world_deceased.sum()
world_deceased = world_sum.values.tolist()

#italy
italy_confirmed = confirmed_cases[confirmed_cases['Country/Region'].str.match('Italy')]
italy_confirmed.drop('Country/Region',axis = 1, inplace=True)
italy_confirmed.drop('Province/State',axis = 1, inplace=True)
copy_for_cdates = italy_confirmed
italy_confirmed = italy_confirmed.values.tolist()[0]

italy_recovered = recovered_cases[recovered_cases['Country/Region'].str.match('Italy')]
italy_recovered.drop('Country/Region',axis = 1, inplace=True)
italy_recovered.drop('Province/State',axis = 1, inplace=True)
copy_for_rdates = italy_recovered
italy_recovered = italy_recovered.values.tolist()[0]

italy_deceased = deceased_cases[deceased_cases['Country/Region'].str.match('Italy')]
italy_deceased.drop('Country/Region',axis = 1, inplace=True)
italy_deceased.drop('Province/State',axis = 1, inplace=True)
copy_for_ddates = italy_deceased
italy_deceased = italy_deceased.values.tolist()[0]

#spain
spain_confirmed = confirmed_cases[confirmed_cases['Country/Region'].str.match('Spain')]
spain_confirmed.drop('Country/Region',axis = 1, inplace=True)
spain_confirmed.drop('Province/State',axis = 1, inplace=True)
spain_confirmed = spain_confirmed.values.tolist()[0]

spain_recovered = recovered_cases[recovered_cases['Country/Region'].str.match('Spain')]
spain_recovered.drop('Country/Region',axis = 1, inplace=True)
spain_recovered.drop('Province/State',axis = 1, inplace=True)
spain_recovered = spain_recovered.values.tolist()[0]

spain_deceased = deceased_cases[deceased_cases['Country/Region'].str.match('Spain')]
spain_deceased.drop('Country/Region',axis = 1, inplace=True)
spain_deceased.drop('Province/State',axis = 1, inplace=True)
spain_deceased = spain_deceased.values.tolist()[0]

#korea
korea_confirmed = confirmed_cases[confirmed_cases['Country/Region'].str.match('Korea')]
korea_confirmed.drop('Country/Region',axis = 1, inplace=True)
korea_confirmed.drop('Province/State',axis = 1, inplace=True)
korea_confirmed = korea_confirmed.values.tolist()[0]

korea_recovered = recovered_cases[recovered_cases['Country/Region'].str.match('Korea')]
korea_recovered.drop('Country/Region',axis = 1, inplace=True)
korea_recovered.drop('Province/State',axis = 1, inplace=True)
korea_recovered = korea_recovered.values.tolist()[0]

korea_deceased = deceased_cases[deceased_cases['Country/Region'].str.match('Korea')]
korea_deceased.drop('Country/Region',axis = 1, inplace=True)
korea_deceased.drop('Province/State',axis = 1, inplace=True)
korea_deceased = korea_deceased.values.tolist()[0]

#sum china
china_confirmed = confirmed_cases[confirmed_cases['Country/Region'].str.match('China')]
china_confirmed.drop('Country/Region',axis = 1, inplace=True)
china_confirmed.drop('Province/State',axis = 1, inplace=True)
china_confirmed_sum = china_confirmed.sum()
china_confirmed = china_confirmed_sum.values.tolist()

china_recovered = recovered_cases[recovered_cases['Country/Region'].str.match('China')]
china_recovered.drop('Country/Region',axis = 1, inplace=True)
china_recovered.drop('Province/State',axis = 1, inplace=True)
china_recovered_sum = china_recovered.sum()
china_recovered = china_recovered_sum.values.tolist()

china_deceased = deceased_cases[deceased_cases['Country/Region'].str.match('China')]
china_deceased.drop('Country/Region',axis = 1, inplace=True)
china_deceased.drop('Province/State',axis = 1, inplace=True)
china_deceased_sum = china_deceased.sum()
china_deceased = china_deceased_sum.values.tolist()

#sum US
us_confirmed = confirmed_cases.copy()
us_confirmed = us_confirmed[confirmed_cases['Country/Region'].str.match('US')]
us_confirmed.drop('Country/Region',axis = 1, inplace=True)
us_confirmed.drop('Province/State',axis = 1, inplace=True)
us_confirmed_sum = us_confirmed.sum()
us_confirmed = us_confirmed_sum.values.tolist()

us_recovered = recovered_cases.copy()
us_recovered = us_recovered[us_recovered['Country/Region'].str.match('US')]
us_recovered.drop('Country/Region',axis = 1, inplace=True)
us_recovered.drop('Province/State',axis = 1, inplace=True)
us_recovered_sum = us_recovered.sum()
us_recovered = us_recovered_sum.values.tolist()

us_deceased = deceased_cases[deceased_cases['Country/Region'].str.match('US')]
us_deceased.drop('Country/Region',axis = 1, inplace=True)
us_deceased.drop('Province/State',axis = 1, inplace=True)
us_deceased_sum = us_deceased.sum()
us_deceased = us_deceased_sum.values.tolist()


'''
#texas
texas_confirmed = confirmed_cases[confirmed_cases['Province/State'].str.match('Texas', na=False)]
texas_confirmed.drop('Country/Region',axis = 1, inplace=True)
texas_confirmed.drop('Province/State',axis = 1, inplace=True)
texas_confirmed = texas_confirmed.values.tolist()

texas_recovered = recovered_cases[recovered_cases['Province/State'].str.match('Texas', na=False)]
texas_recovered.drop('Country/Region',axis = 1, inplace=True)
texas_recovered.drop('Province/State',axis = 1, inplace=True)
texas_recovered = texas_recovered.values.tolist()[0]

texas_deceased = deceased_cases[deceased_cases['Province/State'].str.match('Texas', na=False)]
texas_deceased.drop('Country/Region',axis = 1, inplace=True)
texas_deceased.drop('Province/State',axis = 1, inplace=True)
texas_deceased = texas_deceased.values.tolist()
'''



#fix dates for confirmed CSV 

confirmed_dates = list(copy_for_cdates.columns.values)
fixed_confirmed_dates = list(map(datetime.datetime.strptime, confirmed_dates, len(confirmed_dates)*['%m/%d/%y']))

recovered_dates = list(copy_for_rdates.columns.values)
fixed_recovered_dates = list(map(datetime.datetime.strptime, recovered_dates, len(recovered_dates)*['%m/%d/%y']))

deceased_dates = list(copy_for_ddates.columns.values)
fixed_deceased_dates = list(map(datetime.datetime.strptime, deceased_dates, len(deceased_dates)*['%m/%d/%y']))
print('...complete')



#plot data
print('plotting...')

n = 0

#plot Sim results
n+=1
plt.figure(n)
plt.plot(tday,cum_num_cases,label='cum # cases')
plt.plot(tday,healthcare_use,label='healthcare use')
plt.plot(tday,healthcare_capp, label='healthcare capacity')

plt.title('Simulated Cumulative Number of Cases')
plt.ylabel('#')
plt.xlabel('day #')
plt.legend(loc='best')


#plot Italy
n+=1
plt.figure(n)
plt.plot(fixed_confirmed_dates,italy_confirmed, label='confirmed')
plt.plot(fixed_recovered_dates,italy_recovered, label='recovered')
plt.plot(fixed_deceased_dates,italy_deceased, label='deceased')
plt.title('Italy Cumulative Cases, Recoveries, Deaths')
plt.ylabel('#')
plt.xlabel('date')
plt.legend(loc='best')

#plot Spain
n+=1
plt.figure(n)
plt.plot(fixed_confirmed_dates,spain_confirmed, label='confirmed')
plt.plot(fixed_recovered_dates,spain_recovered, label='recovered')
plt.plot(fixed_deceased_dates,spain_deceased, label='deceased')
plt.title('Spain Cumulative Cases, Recoveries, Deaths')
plt.ylabel('#')
plt.xlabel('date')
plt.legend(loc='best')

#plot Korea
n+=1
plt.figure(n)
plt.plot(fixed_confirmed_dates,korea_confirmed, label='confirmed')
plt.plot(fixed_recovered_dates,korea_recovered, label='recovered')
plt.plot(fixed_deceased_dates,korea_deceased, label='deceased')
plt.title('Korea Cumulative Cases, Recoveries, Deaths')
plt.ylabel('#')
plt.xlabel('date')
plt.legend(loc='best')

#plot China
n+=1
plt.figure(n)
plt.plot(fixed_confirmed_dates,china_confirmed, label='confirmed')
plt.plot(fixed_recovered_dates,china_recovered, label='recovered')
plt.plot(fixed_deceased_dates,china_deceased, label='deceased')
plt.title('China Cumulative Cases, Recoveries, Deaths')
plt.ylabel('#')
plt.xlabel('date')
plt.legend(loc='best')

'''
#Plot Texas
n+=1
plt.figure(n)
plt.plot(fixed_confirmed_dates,texas_confirmed, label='confirmed')
plt.plot(fixed_recovered_dates,texas_recovered, label='recovered')
plt.plot(fixed_deceased_dates,texas_deceased, label='deceased')
plt.title('Texas Cumulative Cases, Recoveries, Deaths')
plt.ylabel('#')
plt.xlabel('date')
plt.legend(loc='best')
'''

#Plot US
n+=1
plt.figure(n)
plt.plot(fixed_confirmed_dates,us_confirmed, label='confirmed')
plt.plot(fixed_recovered_dates,us_recovered, label='recovered')
plt.plot(fixed_deceased_dates,us_deceased, label='deceased')
plt.title('US Cumulative Cases, Recoveries, Deaths')
plt.ylabel('#')
plt.xlabel('date')
plt.legend(loc='best')

#plot World
n+=1
plt.figure(n)
plt.plot(fixed_confirmed_dates,world_confirmed, label='confirmed')
plt.plot(fixed_recovered_dates,world_recovered, label='recovered')
plt.plot(fixed_deceased_dates,world_deceased, label='deceased')
plt.title('World Cumulative Cases, Recoveries, Deaths')
plt.ylabel('#')
plt.xlabel('date')
plt.legend(loc='best')


#plot #cases overlay biased by 'start'
italyx = range(+5,len(italy_confirmed)+5)
usx = range(-7,len(us_confirmed)-7)
koreax = range(+8,len(korea_confirmed)+8)
spainx = range(-5,len(spain_confirmed)-5)
chinax = range(40,len(china_confirmed)+40)

n+=1
plt.figure(n)
plt.plot(italyx, italy_confirmed, label = 'Italy')
plt.plot(usx, us_confirmed, label = 'US')
plt.plot(koreax, korea_confirmed, label = 'Korea')
plt.plot(spainx, spain_confirmed, label = 'Spain')
plt.plot(chinax, china_confirmed, label = 'China')
plt.title('Time-aligned Cumulative # of Cases by Country')
plt.ylabel('#')
plt.xlabel('day #')
plt.legend(loc = 'best')


n+=1
plt.figure(n)
plt.plot(italyx, np.array(italy_confirmed)/60000000, label = 'Italy')
plt.plot(usx, np.array(us_confirmed)/327000000, label = 'US')
plt.plot(koreax, np.array(korea_confirmed)/51000000, label = 'Korea')
plt.plot(spainx, np.array(spain_confirmed)/46000000, label = 'Spain')
plt.plot(chinax, np.array(china_confirmed)/1386000000, label = 'China')
plt.title('Time-aligned Cumulative # of Cases by Country (per capita)')
plt.ylabel('#')
plt.xlabel('day #')
plt.legend(loc = 'best')


#plot #deaths overlay biased by 'start'
italyx = range(-35,len(italy_deceased)-35)
usx = range(-46,len(us_deceased)-46)
koreax = range(-39,len(korea_deceased)-39)
spainx = range(-45,len(spain_deceased)-45)
chinax = range(0,len(china_deceased)+0)

n+=1
plt.figure(n)
plt.plot(italyx, italy_deceased, label = 'Italy')
plt.plot(usx, us_deceased, label = 'US')
plt.plot(koreax, korea_deceased, label = 'Korea')
plt.plot(spainx, spain_deceased, label = 'Spain')
plt.plot(chinax, china_deceased, label = 'China')
plt.title('Time-aligned Cumulative # of Deaths by Country')
plt.ylabel('#')
plt.xlabel('day #')
plt.legend(loc = 'best')


#plot #recoveries overlay biased by 'start'
italyx = range(-32,len(italy_recovered)-32)
usx = range(-50,len(us_recovered)-50)
koreax = range(-37,len(korea_recovered)-37)
spainx = range(-42,len(spain_recovered)-42)
chinax = range(0,len(china_recovered)+0)

n+=1
plt.figure(n)
plt.plot(italyx, italy_recovered, label = 'Italy')
plt.plot(usx, us_recovered, label = 'US')
plt.plot(koreax, korea_recovered, label = 'Korea')
plt.plot(spainx, spain_recovered, label = 'Spain')
plt.plot(chinax, china_recovered, label = 'China')
plt.title('Time-aligned Cumulative # of Recoveries by Country')
plt.ylabel('#')
plt.xlabel('day #')
plt.legend(loc = 'best')



#show all plots
plt.show()




